import {
  JsonConvert,
  OperationMode,
  PropertyConvertingMode,
  PropertyMatchingRule,
  ValueCheckingMode
} from 'json2typescript';

import {environment} from '../../environments/environment';

export function jsonConvertFactory(): JsonConvert {
  const jsonConvert = new JsonConvert()
  if (!environment.production) {
    jsonConvert.operationMode = OperationMode.LOGGING;
  } else {
    jsonConvert.operationMode = OperationMode.ENABLE;
  }
  jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL
  jsonConvert.propertyConvertingMode = PropertyConvertingMode.IGNORE_NULLABLE
  jsonConvert.mapUndefinedToNull = true
  jsonConvert.ignorePrimitiveChecks = false;
  jsonConvert.propertyMatchingRule = PropertyMatchingRule.CASE_STRICT;
  return jsonConvert
}
