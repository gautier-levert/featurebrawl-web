import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {JsonConvert} from 'json2typescript';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {environment} from '../../../environments/environment';
import {FeatureForm} from '../../brawl/components/feature-form/feature-form';
import {Feature} from '../data/feature';

@Injectable({
  providedIn: 'root'
})
export class FeatureService {

  constructor(
    private readonly http: HttpClient,
    private readonly jsonConvert: JsonConvert,
  ) {
  }

  findAll(): Observable<Feature[]> {
    return this.http.get(`${environment.api.baseUrl}/features`, {
      headers: {
        'X-API-Version': '1'
      }
    })
      .pipe(
        map(result => {
          return this.jsonConvert.deserializeArray(result as any[], Feature)
        })
      )
  }

  create(feature: FeatureForm): Observable<Feature> {
    return this.http.post(`${environment.api.baseUrl}/features`, feature, {
      headers: {
        'X-API-Version': '1'
      }
    })
      .pipe(
        map(result => {
          return this.jsonConvert.deserializeObject(result, Feature)
        })
      )
  }

  vote(feature: string, value: number): Observable<any> {
    return this.http.post(`${environment.api.baseUrl}/features/${feature}/vote`, {
      note: value
    }, {
      headers: {
        'X-API-Version': '1'
      }
    })
  }
}
