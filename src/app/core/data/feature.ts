import {JsonObject, JsonProperty, PropertyConvertingMode} from 'json2typescript';

@JsonObject('Feature')
export class Feature {
  @JsonProperty('id', String)
  id: string = '';
  @JsonProperty('name', String)
  name: string = ''
  @JsonProperty('description', String)
  description: string = ''
  @JsonProperty('score', Number)
  score: number = 0
}
