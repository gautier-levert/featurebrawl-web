import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FeatureCreateComponent} from './pages/feature-create/feature-create.component';
import {FeatureListingComponent} from './pages/feature-listing/feature-listing.component';

const routes: Routes = [
  {
    path: '',
    component: FeatureListingComponent,
  },
  {
    path: 'create',
    component: FeatureCreateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrawlRoutingModule {
}
