import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {switchMap} from 'rxjs/operators';

import {FeatureService} from '../../../core/api/feature.service';
import {Feature} from '../../../core/data/feature';

@Component({
  selector: 'app-feature-listing',
  templateUrl: './feature-listing.component.html',
  styleUrls: ['./feature-listing.component.scss']
})
export class FeatureListingComponent implements OnInit, OnDestroy {

  features: Feature[] = []

  private readonly subscription = new Subscription()

  constructor(
    private readonly featureService: FeatureService
  ) {
  }

  ngOnInit(): void {
    this.subscription.add(
      this.featureService.findAll()
        .subscribe(features => {
          this.features = features;
        })
    )
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

  vote(feature: string, value: number): void {
    this.subscription.add(
      this.featureService.vote(feature, value)
        .pipe(
          switchMap(() => this.featureService.findAll()),
        )
        .subscribe(features => {
          this.features = features;
        })
    )
  }
}
