import {Component, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {FeatureService} from '../../../core/api/feature.service';
import {FeatureForm} from '../../components/feature-form/feature-form';

@Component({
  selector: 'app-feature-create',
  templateUrl: './feature-create.component.html',
  styleUrls: ['./feature-create.component.scss']
})
export class FeatureCreateComponent implements OnDestroy {

  private readonly subscription = new Subscription()

  constructor(
    private readonly featureService: FeatureService,
    private readonly router: Router
  ) {
  }

  createFeature(feature: FeatureForm) {
    this.subscription.add(
      this.featureService.create(feature)
        .subscribe(created => {
          console.log(created)
          this.router.navigate(['..'])
        })
    )
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }
}
