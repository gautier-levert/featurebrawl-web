export interface FeatureForm {
  name: string
  description: string
}
