import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Controls, DataInput, NgxRootFormComponent} from 'ngx-sub-form';

import {FeatureForm} from './feature-form';

@Component({
  selector: 'app-feature-form',
  templateUrl: './feature-form.component.html',
  styleUrls: ['./feature-form.component.scss']
})
export class FeatureFormComponent extends NgxRootFormComponent<FeatureForm> {

  @Input('feature')
  @DataInput()
  dataInput: Required<FeatureForm> | null | undefined

  @Output('featureChange')
  dataOutput = new EventEmitter<FeatureForm>()

  constructor() {
    super()
  }

  protected getFormControls(): Controls<FeatureForm> {
    return {
      name: new FormControl(null, Validators.required),
      description: new FormControl(null),
    };
  }

  protected getDefaultValues(): Partial<FeatureForm> | null {
    return {
      name: '',
      description: '',
    };
  }
}
