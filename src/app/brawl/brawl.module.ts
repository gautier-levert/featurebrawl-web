import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {MatBadgeModule} from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';

import {BrawlRoutingModule} from './brawl-routing.module';
import {FeatureFormComponent} from './components/feature-form/feature-form.component';
import {FeatureCreateComponent} from './pages/feature-create/feature-create.component';
import {FeatureListingComponent} from './pages/feature-listing/feature-listing.component';

@NgModule({
  declarations: [
    FeatureListingComponent,
    FeatureCreateComponent,
    FeatureFormComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    BrawlRoutingModule,
    ReactiveFormsModule,

    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatBadgeModule,
  ]
})
export class BrawlModule {
}
