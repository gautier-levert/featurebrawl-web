import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'brawl',
    loadChildren: () => import('./brawl/brawl.module').then(m => m.BrawlModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'brawl'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
